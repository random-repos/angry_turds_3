using UnityEngine;
using System.Collections;

public class TdShotInput {
    Camera mCamera;
    bool mIsClickValid = false;
    Vector3 mMouseDownPosition;

    public TdShotInput(Camera aCamera)
    {
        mCamera = aCamera;
    }
    bool mouseCast(int layer, out RaycastHit hit)
    {
        Ray toCast = mCamera.ViewportPointToRay(getNormalizedMousePosition());
        return Physics.Raycast(toCast, out hit, Mathf.Infinity, layer);
    }

    Vector3 getNormalizedMousePosition()
    {
        Vector3 pos = Input.mousePosition;
        pos.x /= Screen.width;
        pos.y /= Screen.height;
        return pos;
    }
    public bool mouseDown(GameObject target)
    {
        RaycastHit hit;
        if (mouseCast(1<<target.layer, out hit))
        {
            bool valid = false;
            if (target == hit.transform.gameObject)
                valid = true;
            else if (target.rigidbody && hit.collider.attachedRigidbody && target.rigidbody == hit.collider.attachedRigidbody)
                valid = true;
            else if (target.collider && target.collider == hit.collider)
                valid = true;

            if (valid) 
            {
                mMouseDownPosition = getNormalizedMousePosition();
                mIsClickValid = true;
                return true;
            }
        }
        return false;
    }

    public Vector3 mouseUp()
    {
        Vector3 r = getDeviation();
        mIsClickValid = false;
        return r;
    }

    public Vector3 mouseMoved()
    {
        Vector3 r = getNormalizedMousePosition() - mMouseDownPosition;
        mMouseDownPosition = getNormalizedMousePosition();
        return r;
    }

    public bool isShooting() { return mIsClickValid; }
    public Vector3 getDeviation()
    {
        if(!isShooting())
            return Vector3.zero;
        Vector3 cur = getNormalizedMousePosition();
        Vector3 r = Vector3.forward;
        r +=  mMouseDownPosition - cur;
        r.y = 0;
        r.Normalize();
        return -(cur - mMouseDownPosition).magnitude * r;
    }

}
