using UnityEngine;
using System.Collections;

public class TdLauncherSingleton : MonoBehaviour {
    static TdLauncherSingleton inst = null;
    public static TdLauncherSingleton get() { return inst; }

    public GameObject mAxis;
    public GameObject mHandle;
    public GameObject mLaunch;

    // Use this for initialization
    void Awake()
    {
        if (inst == null)
            inst = this;
    }
    

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
