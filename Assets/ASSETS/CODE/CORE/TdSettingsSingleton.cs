using UnityEngine;
using System.Collections;

public class TdSettingsSingleton : MonoBehaviour {
    static TdSettingsSingleton inst = null;
    public static TdSettingsSingleton get() { return inst; }


    public GameObject [] shotObjects;
    public Vector3 gravity = new Vector3(0, -9.8f, 0);
    public float forceScaling = 200;
    public float sideShootingLimits = 0.3f;

    public float timeBetweenShots = 2;

    // Use this for initialization
    void Awake()
    {
        if (inst == null)
            inst = this;
    }

    void Start()
    {
        Physics.gravity = gravity;
    }
}
