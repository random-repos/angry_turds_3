using UnityEngine;
using System.Collections;

public class TdStateSingleton : MonoBehaviour {
    static TdStateSingleton inst = null;
    static public TdStateSingleton get() { return inst; }

    public enum TdGameState
    {
        TD_MENU,
        TD_BEGIN_MENU,
        TD_SHOOT,
        TD_WATCH,
        TD_READY_TO_SHOOT,
        TD_WIN,
        TD_LOSE,
        TD_END_MENU
    }

    TdGameState mState;


    //links to other components

    //slingshot
    TdLauncherSingleton mLauncher;

    //shot related
    GameObject mActiveShotObject = null;
    TdShotInput mShotInput;
    TdShotInput mLaunchAngleInput;
    AnimalTimer mWatchTimer;
    int mShotCount = 0;

    //enemy related
    System.Collections.Generic.HashSet<TdEnemyElement> mEnemies = new System.Collections.Generic.HashSet<TdEnemyElement>();


    public TdGameState getGameState()
    { return mState; }
    public GameObject getActiveShotObject()
    {
        return mActiveShotObject;
    }

    public void registerEnemy(TdEnemyElement aEnemy)
    {
        mEnemies.Add(aEnemy);
    }

    public void enemyDestroyed(TdEnemyElement aEnemey)
    {
        mEnemies.Remove(aEnemey);
        if (mEnemies.Count == 0)
            mState = TdGameState.TD_WIN;
    }

	// Use this for initialization
    void Awake()
    {
        if (inst == null) 
            inst = this;
    }
	void Start () {
        mState = TdGameState.TD_SHOOT;
        mShotInput = new TdShotInput(TdCameraSingleton.get().camera);
        mLaunchAngleInput = new TdShotInput(TdCameraSingleton.get().camera);
        mLauncher = TdLauncherSingleton.get();
        mWatchTimer = new AnimalTimer(0,TdSettingsSingleton.get().timeBetweenShots);
	}
	
	// Update is called once per frame
	void Update () {
        updateToShootObject();
        updateShootingObject();
        updateLauncher();
        inputUpdate();
        cameraInputUpdate();

        //temporary stuff
        if (mState == TdGameState.TD_READY_TO_SHOOT)
            mState = TdGameState.TD_SHOOT;
	}

    void cameraInputUpdate()
    {
        if(mActiveShotObject)
            TdCameraSingleton.get().camera.transform.position += (new Vector3(0,1,0))*(-TdCameraSingleton.get().camera.transform.position.y + mActiveShotObject.transform.position.y);
    }

    void shootObject(Vector3 shotVect)
    {
        mState = TdGameState.TD_WATCH;
        mActiveShotObject.rigidbody.isKinematic = false;
        mActiveShotObject.rigidbody.velocity = Vector3.zero;
        mActiveShotObject.rigidbody.useGravity = true;
        mActiveShotObject.rigidbody.AddForce((mLauncher.mLaunch.transform.position - mLauncher.mLaunch.transform.TransformPoint(shotVect)) * TdSettingsSingleton.get().forceScaling, ForceMode.Impulse);
    }

    void inputUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (mState == TdGameState.TD_SHOOT)
            {
                if (mActiveShotObject)
                    mShotInput.mouseDown(mActiveShotObject);
            }
            mLaunchAngleInput.mouseDown(mLauncher.mHandle);
        }
            
        if (Input.GetMouseButtonUp(0))
        {
            if (mState == TdGameState.TD_SHOOT)
            {
                Vector3 shotVect = mShotInput.mouseUp();
                if (shotVect.magnitude > 0.05f)
                {
                    shotVect.x = Mathf.Clamp(shotVect.x, -TdSettingsSingleton.get().sideShootingLimits, TdSettingsSingleton.get().sideShootingLimits);
                    shotVect.y = Mathf.Clamp(shotVect.y, -1f, 1f);
                    shootObject(shotVect);
                }
            }
            mLaunchAngleInput.mouseUp();
        }
    }
    void updateLauncher()
    {
        if (mLaunchAngleInput.isShooting())
        {
            mLauncher.transform.RotateAround(
                mLauncher.mAxis.transform.position,
                mLauncher.mAxis.transform.up, 
                mLaunchAngleInput.mouseMoved().y * -500);
        }
    }
    void updateToShootObject()
    {   
        //get a new shot object
        if (mActiveShotObject == null && (mState == TdGameState.TD_SHOOT || mState == TdGameState.TD_READY_TO_SHOOT))
        {
            if (mShotCount < TdSettingsSingleton.get().shotObjects.Length)
            {
                mActiveShotObject = TdSettingsSingleton.get().shotObjects[mShotCount];
                mWatchTimer.reset();
                mShotCount++;
            }
        }

        //position it
        if (mActiveShotObject != null && mState == TdGameState.TD_SHOOT)
        {
            Vector3 pos = mShotInput.getDeviation();
            pos.x = Mathf.Clamp(pos.x, -TdSettingsSingleton.get().sideShootingLimits/2.0f, TdSettingsSingleton.get().sideShootingLimits/2.0f);
            pos.y = Mathf.Clamp(pos.y, -0.5f, 0.5f);
            pos *= 2;
            mActiveShotObject.transform.position = mLauncher.mLaunch.transform.TransformPoint(pos);
            mActiveShotObject.transform.rotation = mLauncher.transform.rotation;
            mActiveShotObject.rigidbody.useGravity = false;
        }
    }

    void updateShootingObject()
    {
        if (mActiveShotObject != null && mState == TdGameState.TD_WATCH)
        {
            mWatchTimer.update(Time.deltaTime);
            if (mWatchTimer.isExpired())
            {
                mActiveShotObject = null; //done with this one
                //TODO create a destroy timer of sorts on the shot object
                mState = TdGameState.TD_READY_TO_SHOOT;

            }
        }
    }
}
