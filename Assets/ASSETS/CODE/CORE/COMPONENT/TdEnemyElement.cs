using UnityEngine;
using System.Collections;

public class TdEnemyElement : MonoBehaviour {
    public AudioClip toPlayOnDestroy;
    AudioSource mSource;

	// Use this for initialization
	void Start () {
        TdStateSingleton.get().registerEnemy(this);
        if (audio)
            mSource = audio;
        else mSource = gameObject.AddComponent<AudioSource>();
	}

    void amDestroyed()
    {
        mSource.PlayOneShot(toPlayOnDestroy);
        TdStateSingleton.get().enemyDestroyed(this);
    }
    void hitMe()
    {
        Destroy(this.gameObject,0.5f);
        amDestroyed();
    }

    public float destroyForce = 999;
    public float destroyVelocity = 5;


    void OnCollisionEnter(Collision collision)
    {
        foreach (ContactPoint contact in collision.contacts)
        {
            Debug.DrawRay(contact.point, contact.normal, Color.white);
        }
        if (collision.impactForceSum.magnitude > destroyForce || collision.relativeVelocity.magnitude > destroyVelocity)
        {
            hitMe();
        }
    }
}
