using UnityEngine;
using System.Collections;

public class TdSetTextOnWin : MonoBehaviour {
    public TextMesh text;
    public string winText;
	public void FixedUpdate () 
    {
        if (TdStateSingleton.get().getGameState() == TdStateSingleton.TdGameState.TD_WIN)
        {
            text.text = winText;
        }
        else
        {
            text.text = "";
        }
	}
}
