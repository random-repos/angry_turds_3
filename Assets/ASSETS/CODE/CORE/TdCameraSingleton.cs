using UnityEngine;
using System.Collections;

public class TdCameraSingleton : MonoBehaviour {
    public Transform shotLocation;
    public Transform scenicLocation;

    static TdCameraSingleton inst = null;
    public static TdCameraSingleton get() { return inst; }
    void Awake()
    {
        if (inst == null)
            inst = this;
    }
    enum TdCameraState
    {
        TD_CAM_SHOT_POV,
        TD_CAM_TRACKING,
        TD_CAM_FIXED
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        TdStateSingleton.TdGameState state = TdStateSingleton.get().getGameState();
        if (state == TdStateSingleton.TdGameState.TD_SHOOT)
        {
            camera.transform.position = shotLocation.position;
            camera.transform.rotation = shotLocation.rotation;
            if(TdStateSingleton.get().getActiveShotObject())
                camera.transform.position += (new Vector3(0, 1, 0)) * (-camera.transform.position.y + TdStateSingleton.get().getActiveShotObject().transform.position.y);
        }
        else if (state == TdStateSingleton.TdGameState.TD_WATCH)
        {
            camera.transform.position = scenicLocation.position;
            camera.transform.rotation = scenicLocation.rotation;
        }
	}
}
