using UnityEngine;
using System.Collections;

public class TdStageElement : MonoBehaviour {

    public float destroyForce = 100;
    public float destroyVelocity = 100;

    
    void OnCollisionEnter(Collision collision)
    {
        foreach (ContactPoint contact in collision.contacts)
        {
            Debug.DrawRay(contact.point, contact.normal, Color.white);
        }
        if (collision.impactForceSum.magnitude > 100 || collision.relativeVelocity.magnitude > 100)
        {

        }
    }
}
