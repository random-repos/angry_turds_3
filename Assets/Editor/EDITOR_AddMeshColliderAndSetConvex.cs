using UnityEngine;
using UnityEditor;
using System.Collections;

public class EDITOR_AddMeshColliderAndSetConvex : Editor
{
    [MenuItem("GameObject/Add Mesh Collider And Set Convex Recursively", true)]
    static bool validateAddComponents()
    {
        return Selection.activeGameObject != null;
    }
    [MenuItem("GameObject/Add Mesh Collider And Set Convex Recursively")]
    static void AddComponentsRecursivelyItem()
    {
        RecurseAndAdd(Selection.activeGameObject.transform);
    }

    static int RecurseAndAdd(Transform parent)
    {
        //keep count
        int total = 0;
        //add components to children
        foreach (Transform child in parent)
        {
            total += RecurseAndAdd(child);
        }
        //add component to parent
        MeshCollider col = parent.GetComponent<MeshCollider>();
        bool flag = false;
        if (!col)
        {
            col = parent.gameObject.AddComponent<MeshCollider>();
            flag = true;
        }
        col.convex = true;
        if (flag) total++;
        return total;
    }
}
