using UnityEngine;
using UnityEditor;
using System.Collections;

public class EDITOR_AddRigidBodyAndMeshCollider : Editor
{
    [MenuItem("GameObject/Add Rigid Body And Mesh Collider",true)]
    static bool validateAddComponents()
    {
        return Selection.activeGameObject != null;
    }
    [MenuItem("GameObject/Add Rigid Body And Mesh Collider")]
    static void AddComponentsRecursivelyItem() 
    {
        RecurseAndAdd(Selection.activeGameObject.transform);
    }
    
    static int RecurseAndAdd(Transform parent)
    {
        //keep count
        int total = 0;
        //add components to children
        foreach (Transform child in parent) {
            total += RecurseAndAdd(child);
        }
        //add component to parent
        Rigidbody rb = parent.GetComponent<Rigidbody>();
        Component col = parent.GetComponent<MeshCollider>();
        bool flag = false;
        if (!rb)
        {
            Rigidbody r = parent.gameObject.AddComponent<Rigidbody>();
            r.isKinematic = false;
            r.useGravity = false;
            flag = true;
        }
        if (!col)
        {
            parent.gameObject.AddComponent<MeshCollider>();
            flag = true;
        }

        if (flag) total++;
        return total;
    }
}
